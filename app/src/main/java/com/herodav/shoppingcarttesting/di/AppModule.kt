package com.herodav.shoppingcarttesting.di

import android.content.Context
import androidx.room.Room
import com.herodav.shoppingcarttesting.data.DefaultShoppingRepository
import com.herodav.shoppingcarttesting.data.IShoppingRepository
import com.herodav.shoppingcarttesting.data.local.ShoppingItemDao
import com.herodav.shoppingcarttesting.data.local.ShoppingItemDb
import com.herodav.shoppingcarttesting.data.remote.PixabayService
import com.herodav.shoppingcarttesting.utils.Constants.BASE_URL
import com.herodav.shoppingcarttesting.utils.Constants.DB_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideShoppingItemDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, ShoppingItemDb::class.java, DB_NAME).build()

    @Singleton
    @Provides
    fun provideShoppingItemDao(database: ShoppingItemDb) = database.shoppingItemDao()

    @Singleton
    @Provides
    fun provideDefaultShoppingRepository(
        dao: ShoppingItemDao,
        pixabayService: PixabayService
    ) = DefaultShoppingRepository(dao, pixabayService) as IShoppingRepository

    @Singleton
    @Provides
    fun providePixabayService(): PixabayService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(PixabayService::class.java)
    }
}