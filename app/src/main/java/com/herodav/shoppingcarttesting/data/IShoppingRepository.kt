package com.herodav.shoppingcarttesting.data

import androidx.lifecycle.LiveData
import com.herodav.shoppingcarttesting.data.local.ShoppingItem
import com.herodav.shoppingcarttesting.data.remote.responses.ImageResponse
import retrofit2.Response

/*
* Holds all the methods for Shopping Repositories
*
* */
interface IShoppingRepository {

    suspend fun insertShoppingItems(vararg shoppingItems: ShoppingItem)

    suspend fun deleteShoppingItem(shoppingItem: ShoppingItem)

    fun observeAllShoppingItems(): LiveData<List<ShoppingItem>>

    fun observeTotalPrice(): LiveData<Float>

    suspend fun searchForImage(imageQuery: String): Resource<ImageResponse>
}