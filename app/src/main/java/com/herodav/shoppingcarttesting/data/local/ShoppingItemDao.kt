package com.herodav.shoppingcarttesting.data.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ShoppingItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllShoppingItems(vararg shoppingItem: ShoppingItem)

    @Delete
    suspend fun deleteShoppingItem(ShoppingItem: ShoppingItem)

    @Query("SELECT * FROM shopping_item ORDER BY id")
    fun observeAllShoppingItems(): LiveData<List<ShoppingItem>>

    @Query("SELECT SUM(price * amount) FROM shopping_item")
    fun observeTotalPrice():LiveData<Float>
}