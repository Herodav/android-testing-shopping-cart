package com.herodav.shoppingcarttesting.data.remote

import com.herodav.shoppingcarttesting.utils.Constants.API_KEY
import com.herodav.shoppingcarttesting.data.remote.responses.ImageResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PixabayService {

    @GET("/api/")
    suspend fun searchForImage(
        @Query("q") searchQuery: String,
        @Query("key") apiKey: String = API_KEY
    ): Response<ImageResponse>

}