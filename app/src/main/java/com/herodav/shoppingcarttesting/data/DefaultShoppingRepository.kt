package com.herodav.shoppingcarttesting.data

import androidx.lifecycle.LiveData
import com.herodav.shoppingcarttesting.data.local.ShoppingItem
import com.herodav.shoppingcarttesting.data.local.ShoppingItemDao
import com.herodav.shoppingcarttesting.data.remote.PixabayService
import com.herodav.shoppingcarttesting.data.remote.responses.ImageResponse
import com.herodav.shoppingcarttesting.utils.Constants.ERROR_IMAGE_NOT_FOUND
import java.lang.Exception
import javax.inject.Inject

/**Generic Repo class*/
class DefaultShoppingRepository @Inject constructor(
    private val shoppingItemDao: ShoppingItemDao,
    private val pixabayService: PixabayService
) : IShoppingRepository {
    override suspend fun insertShoppingItems(vararg shoppingItems: ShoppingItem) {
        shoppingItemDao.insertAllShoppingItems(*shoppingItems)
    }

    override suspend fun deleteShoppingItem(shoppingItem: ShoppingItem) {
        shoppingItemDao.deleteShoppingItem(shoppingItem)
    }

    override fun observeAllShoppingItems(): LiveData<List<ShoppingItem>> {
        return shoppingItemDao.observeAllShoppingItems()
    }

    override fun observeTotalPrice(): LiveData<Float> {
        return shoppingItemDao.observeTotalPrice()
    }

    override suspend fun searchForImage(imageQuery: String): Resource<ImageResponse> {
        val response = pixabayService.searchForImage(imageQuery)
        return if (response.isSuccessful) {
            response.body()?.let {
                Resource.success(it)
            } ?: Resource.error(ERROR_IMAGE_NOT_FOUND, null)
        } else {
            Resource.error(response.message().toString(), null)
        }
    }
}