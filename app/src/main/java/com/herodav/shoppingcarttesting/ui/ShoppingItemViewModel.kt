package com.herodav.shoppingcarttesting.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.herodav.shoppingcarttesting.data.IShoppingRepository
import com.herodav.shoppingcarttesting.data.Resource
import com.herodav.shoppingcarttesting.data.local.ShoppingItem
import com.herodav.shoppingcarttesting.data.remote.responses.ImageResponse
import com.herodav.shoppingcarttesting.utils.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShoppingItemViewModel @Inject constructor(
    private val shoppingRepository: IShoppingRepository
) : ViewModel() {

    val shoppingItems = shoppingRepository.observeAllShoppingItems()

    val totalPrice = shoppingRepository.observeTotalPrice()

    private val _images = MutableLiveData<Event<Resource<ImageResponse>>>()
    val images: LiveData<Event<Resource<ImageResponse>>> = _images

    private val _currentImageUrl = MutableLiveData<String>()
    val currentImageUrl: LiveData<String> = _currentImageUrl

    private val _insertShoppingItemsStatus = MutableLiveData<Event<Resource<ShoppingItem>>>()
    val insertShoppingItemsStatus: LiveData<Event<Resource<ShoppingItem>>> =
        _insertShoppingItemsStatus

    fun setCurrentImageUrl(url: String) {
        _currentImageUrl.postValue(url)
    }

    fun deleteShoppingItem(shoppingItem: ShoppingItem) = viewModelScope.launch{
        shoppingRepository.deleteShoppingItem(shoppingItem)
    }

    fun insertShoppingItemsIntoDb(vararg shoppingItems: ShoppingItem) = viewModelScope.launch {
        shoppingRepository.insertShoppingItems(*shoppingItems)
    }

    fun insertShoppingItem(name: String, amountString: String, priceString: String){
    }

    fun searchForImage(imageQuery: String) {
    }
}