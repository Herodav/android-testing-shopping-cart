package com.herodav.shoppingcarttesting.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

abstract class BaseFragment(layout: Int): Fragment(layout) {

    lateinit var shoppingItemViewModel: ShoppingItemViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //ViewModel attached to activity cause it's shared among all fragments
        shoppingItemViewModel = ViewModelProvider(requireActivity()).get(ShoppingItemViewModel::class.java)
    }
}