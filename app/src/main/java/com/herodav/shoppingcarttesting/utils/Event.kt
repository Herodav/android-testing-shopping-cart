package com.herodav.shoppingcarttesting.utils

/**
 * Makes LiveData emit onetime events
 * If for instance the screen rotates when the LiveData value is Result.Error()
 * And we had to display a snackbar in this case,
 * Upon rotation the error would be emitted again and the snackbar would be displayed for the second time
 * This class ensures such behaviour is prevented
 *
 */
open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}