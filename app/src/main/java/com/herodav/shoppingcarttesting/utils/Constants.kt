package com.herodav.shoppingcarttesting.utils

import com.herodav.shoppingcarttesting.BuildConfig

object Constants {
    const val BASE_URL = "https://pixabay.com"
    const val API_KEY = BuildConfig.API_KEY

    const val DB_NAME = "shopping_db"

    //errors msg
    const val ERROR_IMAGE_NOT_FOUND = "Sorry, no image matched your search"

    const val MAX_NAME_LENGTH = 20
    const val MAX_PRICE_LENGTH = 10
}