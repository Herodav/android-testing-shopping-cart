package com.herodav.shoppingcarttesting

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShoppingCartApplication : Application(){

}