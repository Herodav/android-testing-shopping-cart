package com.herodav.shoppingcarttesting.data

import junit.framework.TestCase

import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class DefaultShoppingRepositoryTest : TestCase() {

    @Before
    public override fun setUp() {
    }

    @After
    public override fun tearDown() {
    }

    @Test
    fun insertShoppingItems() {
    }

    @Test
    fun deleteShoppingItem() {
    }

    @Test
    fun observeAllShoppingItems() {
    }

    @Test
    fun observeTotalPrice() {
    }

    @Test
    fun searchForImage() {
    }
}