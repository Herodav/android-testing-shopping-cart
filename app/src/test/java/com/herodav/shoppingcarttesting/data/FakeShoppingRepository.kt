package com.herodav.shoppingcarttesting.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.herodav.shoppingcarttesting.data.local.ShoppingItem
import com.herodav.shoppingcarttesting.data.remote.responses.ImageResponse

class FakeShoppingRepository : IShoppingRepository {

    private val shoppingItemList = mutableListOf<ShoppingItem>()

    private val observableShoppingItems = MutableLiveData<List<ShoppingItem>>(shoppingItemList)
    private val observableTotalPrice = MutableLiveData<Float>()

    private var shouldReturnNetworkError = false

    fun setShouldReturnNetworkError(value: Boolean) {
        shouldReturnNetworkError = value
    }

    private fun refreshLiveData() {
        observableShoppingItems.postValue(shoppingItemList)
        observableTotalPrice.postValue(getTotalPrice())
    }

    private fun getTotalPrice() = shoppingItemList.sumByDouble { it.price.toDouble() * it.amount.toDouble() }.toFloat()

    override suspend fun insertShoppingItems(vararg shoppingItems: ShoppingItem) {
        shoppingItemList.addAll(shoppingItems)
        refreshLiveData()
    }

    override suspend fun deleteShoppingItem(shoppingItem: ShoppingItem) {
        shoppingItemList.remove(shoppingItem)
        refreshLiveData()
    }

    override fun observeAllShoppingItems(): LiveData<List<ShoppingItem>> {
        return observeAllShoppingItems()
    }

    override fun observeTotalPrice(): LiveData<Float> {
        return observeTotalPrice()
    }

    override suspend fun searchForImage(imageQuery: String): Resource<ImageResponse> {
        return if (shouldReturnNetworkError){
            Resource.error("Error", null)
        }else{
            Resource.success(ImageResponse(listOf(), 0, 0))
        }
    }
}