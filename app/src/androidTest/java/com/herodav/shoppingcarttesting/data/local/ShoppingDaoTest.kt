package com.herodav.shoppingcarttesting.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.*
import com.herodav.shoppingcarttesting.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi //because we're using runBlockingTest{}
@RunWith(AndroidJUnit4::class)
@SmallTest //cause it's ui test
class ShoppingDaoTest {

    @get: Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: ShoppingItemDb
    private lateinit var dao: ShoppingItemDao

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            ShoppingItemDb::class.java
        ).allowMainThreadQueries().build()

        dao = database.shoppingItemDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun insertShoppingItem() = runBlockingTest {
        val shoppingItem = ShoppingItem("name", 1, 5.50f, "url", id=1)
        dao.insertAllShoppingItems(shoppingItem)

        val allShoppingItems = dao.observeAllShoppingItems().getOrAwaitValue()

        assertThat(allShoppingItems).contains(shoppingItem)
    }

    @Test
    fun deleteShoppingItem() = runBlockingTest {
        val shoppingItem = ShoppingItem("name", 1, 5.50f, "url", id=1)
        dao.insertAllShoppingItems(shoppingItem)
        dao.deleteShoppingItem(shoppingItem)
        val allShoppingItems = dao.observeAllShoppingItems().getOrAwaitValue()

        assertThat(allShoppingItems).doesNotContain(shoppingItem)
    }

    @Test
    fun observeTotalPrice() = runBlockingTest{
        val shoppingItem1 = ShoppingItem("name1", 1, 1.00f, "url", id=1)
        val shoppingItem2 = ShoppingItem("name2", 4, 2.00f, "url", id=2)
        val shoppingItem3 = ShoppingItem("name3", 6, 3.00f, "url", id=3)
        val shoppingItem4 = ShoppingItem("name4", 0, 4.00f, "url", id=4)

        val itemsList = listOf(shoppingItem1, shoppingItem2, shoppingItem3, shoppingItem4)
        dao.insertAllShoppingItems(*itemsList.toTypedArray())

        val totalPrice = dao.observeTotalPrice().getOrAwaitValue()
        assertThat(totalPrice).isEqualTo(getItemsSum(itemsList))
    }

    private fun getItemsSum(shoppingItems: List<ShoppingItem>) = shoppingItems.map { it.price * it.amount }.sum()
}